Hands-free Recipes
==================

Software
--------
### KRecipes
https://www.kde.org/applications/utilities/krecipes/

TODO: Kompilieren und ausprobieren

### Gnome-Recipes
    sudo zypper install gnome-recipes

Mit Vollbild-Schritt-für-Schritt und Timern, aber keine Sprachausgabe

### MyCroft
https://mycroft.ai/get-mycroft/

TODO: Kompilieren und installieren: siehe https://github.com/MycroftAI/mycroft-core/blob/dev/README.md

### Existierende Lösungen
* https://www.theverge.com/2016/11/21/13696992/alexa-echo-recipe-skill-allrecipes, 2016
    * Video deutet gegen Ende an, dass man auch einzelne Schritte aufrufen kann
* http://chefbasil.co/
* https://www.liip.ch/en/blog/betti-bossi-recipe-assistant-prototype-with-automatic-speech-recognition-asr-and-text-to-speech-tts-on-socket-io, 2018
    * Prototype

Idee
----
* MyCroft mit KRecipes verbinden
* Text-to-Speech mit Qt Speech oder mit MyCroft
* Komfortfunktion mit gesprochenden Rezepten (z. B. von speziellem Vegan-Blog, welcher?) ähnlich wie Lyrics

Plan
----
1. Vorhandene Software installieren und ausprobieren.
2. Wie bekommt man ein neues Rezept in die Datenbank?
3. Was sind die Mindestanforderungen? Braucht es Timer?
    - "Nächster Schritt"
    - "Schritt zurück"
    - "5 Sekunden zurück" (falls alles ein großer Text)
    - "Pause"
    - "Zutatenliste vorlesen"?
